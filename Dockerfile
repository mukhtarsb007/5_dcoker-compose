# Базовый образ Python 3.8
FROM python:3.8-slim AS base

# Установка зависимостей


# Устанавливаем рабочую директорию
WORKDIR /app

# Копируем файлы проекта в рабочую директорию
COPY . /app

# Устанавливаем зависимости
RUN pip install -r requirements.txt

# Передаем переменную FLASK_APP
ENV FLASK_APP=src/app.py


RUN chmod +x docker-entrypoint.sh 
# Команда запуска приложения
CMD ["bash","docker-entrypoint.sh"]
